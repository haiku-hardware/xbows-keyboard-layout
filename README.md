# XBows Keyboard Layout
---

X-Bows 86 key mechanical programmable ergonomic keyboard
Keyboard Layout file for Haiku

The file 'X-Bows Nature' is the KeyboardLayout file needed to match the layout of the X-Bows keyboard on Haiku.
The layout matches that of the X-Bows keyboard though the angles are not visible in the layout, and key shapes are always rectangular in the layout while they have different shapes in the actual keyboard.

This layout works fine on Haiku, I've been testing it before uploading here.

It might have some improvements in the future though, if I figure out (if possible) to match the angle rotation of keys.

![alt text][logo]

[logo]:Xbows_haikuLayout.png
